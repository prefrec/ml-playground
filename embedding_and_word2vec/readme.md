## Installation

Ensure Python and pip are installed on your system

`pip install scipy gensim numpy`

## Usage

python parse_and_embed.py

## Description

A simple test script for generating word2vec style embeddings for tags in the danbooru dataset, and using them to learn the user's preferences.

## Requirements
python2.7 with these extra packages:
    numpy
    scipy
    gensim

the danbooru metadata json files (acquired here: https://www.kaggle.com/mylesoneill/tagged-anime-illustrations/home) - NOTE: You do not need the image files

The default path is D:\danbooru-metadata\*.json, you can change that in the code (only two places)

-----------------------------------------------------------------

## How it works:

1 - take one of the json files, parse all the tags for each image into an array 

2 - use word2vec to learn the embedding with each image's tags being treated as one sentence

3 - take another of the json files, and use the embedding to look up the tags

4 - sum the emdedded tags for each image. This results in each image having a 100d vector associated with it that is representative of its content

5 - Begin the user testing

5.1 - create a user vector with random value

5.2 - find an image that is close to the user vector value (cosine distance). If an image can't be found, increase the threshold at which an  image is acceptable.

5.3 - ask the user if they hate/dislike/like/love the image. 

5.4 - add the image vector multiplied by the hate-love factor (-1,-0.5,0.5,1 respectively) to the user's vector

5.5 - GOTO 5.2