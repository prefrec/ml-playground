import json
import numpy as np
from scipy import spatial
import webbrowser
from gensim.models import Word2Vec
from datetime import datetime


trainfile = open("D:\\danbooru-metadata\\201700.json",'r')
dataset = []
print("[%s] Parsing trainfile: %s"%(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3],trainfile))
while True:
    try:
        imageline = trainfile.readline()
        jsonline = json.loads(imageline)
    except:
        break; #super lazy way of detecting eof
    tags = jsonline["tags"]
    link = "https://danbooru.donmai.us/posts/"+jsonline["id"]
    taglist = []
    for tag in tags:
        tagstring = str(tag['name'])
        splittags = tagstring.replace("_","-").split("-")
        for ptag in splittags:
            taglist.append(ptag)        
    
    dataset.append(taglist)  

print("[%s] Creating embeddings..."%(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]))    

model = Word2Vec(dataset, min_count=1)
model.save('model.bin')
print("[%s] Done creating embedding!"%(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]))
print(model)
dataset = []
testfile = open("D:\\danbooru-metadata\\201701.json",'r')
print("[%s] Parsing testfile: %s"%(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3],trainfile))
missingCount = 0
foundCount = 0
while True:
    try:
        imageline = testfile.readline()
        jsonline = json.loads(imageline)
    except:
        break; #super lazy way of detecting eof
    tags = jsonline["tags"]
    link = "https://danbooru.donmai.us/posts/"+jsonline["id"]
    taglist = []
    for tag in tags:
        tagstring = str(tag['name'])
        splittags = tagstring.replace("_","-").split("-")
        #print(splittags)
        for ptag in splittags:
            try:
                taglist.append(model[ptag])
                foundCount = foundCount + 1
            except KeyError:
                taglist.append(np.zeros(100,dtype=np.float32))
                missingCount = missingCount + 1
    
    dataset.append([link,taglist])  
    #print(dataset[0])

print("[%s] Missing %d of %d words from embedding"%(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3],missingCount,missingCount+foundCount))
for example in dataset:
    total_embedding = np.zeros(100,dtype=np.float32)
    for tag in example[1]:
        total_embedding = total_embedding + tag
    total_embedding = total_embedding / np.sqrt(np.sum(total_embedding**2)) #normalise
    example[1] = total_embedding
print("[%s] Finding some examples to test"%(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]))
userVector = np.random.rand(100)
while True:
    #pick one at random    
    distance = 2
    attempts = 0
    while distance > 0.05*attempts/20:
        attempts = attempts + 1
        pick = dataset[np.random.randint(0,len(dataset))]
        distance = spatial.distance.cosine(pick[1],userVector)
        
    print("[%s] Found: %s Response? [0: hate 1:dislike 2:like 3:love] (distance %g)"%(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3],pick[0],distance))    
    webbrowser.open(pick[0], new=2)
    response = raw_input("Response: ")
    if response == "0":
        userVector = userVector + (-1.0*pick[1])
    elif response == "1":
        userVector = userVector + (-0.5*pick[1])
    elif response == "2":
        userVector = userVector + (0.5*pick[1])
    elif response == "3":
        userVector = userVector + (1.0*pick[1])    
    userVector = userVector / np.sqrt(np.sum(userVector**2)) #normalise
    print(userVector)        

    