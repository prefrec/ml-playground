package main

// parameter -b flag to build similarity tables
// parameter -l to print links

import (
	"bufio"
	"encoding/csv"
	"encoding/gob"
	"errors"
	"flag"
	"fmt"
	"log"
	"math"
	"math/bits"
	"os"
	"sort"
	"strings"
)

const (
	dataFile    = "sampled_danbooru_data.csv"
	urlPrefix   = "https://danbooru.donmai.us/posts/" // + colID
	output      = "out.txt"
	tableLoc    = "./table.gob"
	refLoc      = "./ref.gob"
	invRefLoc   = "./invRef.gob"
	colID       = 1
	colTag      = 13
	numFound    = 100
	numTags     = 2000 // Must rebuild tables after changing this
	threshold   = 0.99 // Must rebuild tables after changing this
	suggestions = 10
	size        = 64
)

func main() {
	build := flag.Bool("b", false, "to build and serialize gob tables")
	links := flag.Bool("l", false, "to print tag links")
	flag.Parse()
	if *build {
		buildTables()
	}
	var ref, invRef, matrix = loadTables()
	if *links {
		printLinks(matrix, invRef)
	}
	data := loadData(dataFile)
	rows := loadRows(data)
	rowTags := getRowTags(rows, ref, getBitTagsExpanded(matrix))
	var sb strings.Builder
	for i := 0; i < numFound; i++ {
		ranks := make([]rank, len(rows))
		for j := range rows {
			ranks[j] = rank{
				row:   j,
				score: getScore(rowTags[i], rowTags[j]),
			}
		}
		sort.Slice(ranks, func(a, b int) bool { return ranks[a].score > ranks[b].score })
		sb.WriteString(fmt.Sprintf("found for %s%s\n", urlPrefix, data[i][colID]))
		for q := 1; q < suggestions; q++ {
			sb.WriteString(fmt.Sprintf("%.3f\t%s%s\n", ranks[q].score, urlPrefix, data[ranks[q].row][colID]))
		}
		sb.WriteString(fmt.Sprintf("\n"))
	}
	writeToOut(sb)
	fmt.Println("OK")
}

func getScore(a, b *bitset) float64 {
	truePos := a.truePos(b).popcnt()
	trueNeg := a.trueNeg(b).popcnt()
	falsePos := a.falsePos(b).popcnt()
	falseNeg := a.falseNeg(b).popcnt()
	score := mcc(truePos, trueNeg, falsePos, falseNeg)
	if math.IsNaN(score) {
		score = math.Inf(-1)
	}
	return score
}

func getRowTags(rows [][]string, ref map[string]int, tags []*bitset) []*bitset {
	rowTags := make([]*bitset, len(rows))
	for i, row := range rows {
		rowTags[i] = makeBitset()
		for _, tag := range row {
			if tagIndex, ok := ref[tag]; ok {
				rowTags[i] = rowTags[i].union(tags[tagIndex])
			}
		}
	}
	return rowTags
}

func writeToOut(sb strings.Builder) {
	f, err := os.Create(output)
	defer f.Close()
	if err != nil {
		errorExit(err)
		return
	}
	_, err = f.WriteString(sb.String())
	if err != nil {
		errorExit(err)
		return
	}
}

func printLinks(m [numTags][numTags]float64, invRef []string) {
	for i, row := range m {
		for j, x := range row {
			if i != j && x >= threshold {
				fmt.Println(invRef[i], invRef[j])
			}
		}
	}
}

func getBitTagsExact() []*bitset {
	tagBits := make([]*bitset, numTags)
	for i := 0; i < numTags; i++ {
		t := makeBitset()
		t.set(uint(i))
		tagBits[i] = t
	}
	return tagBits
}

// A tag's bitset contains bits for whether or not a tag matches it at that
// bit's index.
func getBitTagsExpanded(m [numTags][numTags]float64) []*bitset {
	tagBits := make([]*bitset, len(m))
	for i, row := range m {
		t := makeBitset()
		for j, x := range row {
			if x >= threshold || i == j {
				t.set(uint(j))
			}
		}
		tagBits[i] = t
	}
	return tagBits
}

func loadTables() (map[string]int, []string, [numTags][numTags]float64) {
	var ref = make(map[string]int)
	var invRef = make([]string, numTags)
	var matrix [numTags][numTags]float64
	err := readGob(refLoc, &ref)
	if err != nil {
		errorExit(err)
	}
	err = readGob(invRefLoc, &invRef)
	if err != nil {
		errorExit(err)
	}
	err = readGob(tableLoc, &matrix)
	if err != nil {
		errorExit(err)
	}
	return ref, invRef, matrix
}

func buildTables() {
	rows := loadRows(loadData(dataFile))
	list := makeTagList(rows)
	ref := make(map[string]int)
	invRef := make([]string, len(list))
	for i, v := range list {
		ref[v.tag] = i
		invRef[i] = v.tag
	}
	tagMatrix, rowFreq := populateTagMatrix(rows, ref)
	cosMatrix := getStandMatrix(getCosMatrix(tagMatrix))
	tfidfMatrix := getTFIDFMatrix(cosMatrix, float64(len(rows)), rowFreq)
	standMatrix := getStandMatrix(tfidfMatrix)
	cleanMatrix(standMatrix)

	err := writeGob(tableLoc, standMatrix)
	if err != nil {
		errorExit(err)
	}
	err = writeGob(refLoc, ref)
	if err != nil {
		errorExit(err)
	}
	err = writeGob(invRefLoc, invRef)
	if err != nil {
		errorExit(err)
	}
}

// Ensuring links are bi-directional.
func cleanMatrix(m [numTags][numTags]float64) {
	for i := range m {
		for j := range m[i] {
			if m[i][j] >= threshold && m[j][i] < threshold {
				m[i][j] = 0
				m[j][i] = 0
			}
		}
	}
}

func getStandMatrix(m [numTags][numTags]float64) [numTags][numTags]float64 {
	var nM [numTags][numTags]float64
	stand := make([]percInd, 0, numTags*numTags)
	freq := make(map[float64]int, numTags*numTags)
	const prec = 0.00000001
	for i := range m {
		for j, x := range m[i] {
			value := math.Round(x/prec) * prec
			freq[value]++
			stand = append(stand, percInd{
				x:   i,
				y:   j,
				val: value,
			})
		}
	}
	sort.Slice(stand, func(i, j int) bool { return stand[i].val < stand[j].val })
	for i := 1; i < len(stand); i++ {
		if stand[i-1].val == stand[i].val {
			stand[i].numBelow = stand[i-1].numBelow
		} else {
			stand[i].numBelow = stand[i-1].numBelow + freq[stand[i-1].val]
		}
	}
	for _, p := range stand {
		frequency := float64(freq[p.val])
		below := float64(p.numBelow)
		percentile := (below + frequency*0.5) / float64(len(stand))
		nM[p.x][p.y] = percentile
	}
	return nM
}

func getCosMatrix(m [numTags][numTags]float64) [numTags][numTags]float64 {
	var cM [numTags][numTags]float64
	for i := range m {
		for j := range m[i] {
			cM[i][j] = findCos(m[i], m[j])
		}
	}
	return cM
}

func findCos(a [numTags]float64, b [numTags]float64) float64 {
	var dpAB, dpAA, dpBB float64
	for i := range a {
		dpAB += a[i] * b[i]
		dpAA += a[i] * a[i]
		dpBB += b[i] * b[i]
	}
	return dpAB / (math.Sqrt(dpAA) * math.Sqrt(dpBB))
}

func getTFIDFMatrix(m [numTags][numTags]float64, n float64, rowFreq []int) [numTags][numTags]float64 {
	var tM [numTags][numTags]float64
	for i := range m {
		var sum, sumA, sumB float64
		for j := range m[i] {
			sumA += m[i][j]
			sumB += m[j][i]
		}
		if sumA > sumB {
			sum = sumA
		} else {
			sum = sumB
		}
		for j, x := range m[i] {
			var max int
			if rowFreq[i] > rowFreq[j] {
				max = rowFreq[i]
			} else {
				max = rowFreq[j]
			}
			tf := x / sum
			idf := math.Log(n / float64(max))
			tM[i][j] = tf * idf
		}
	}
	return tM
}

// Count number of times a tag occurs with another tag within an item.
func populateTagMatrix(rows [][]string, ref map[string]int) ([numTags][numTags]float64, []int) {
	var m [numTags][numTags]float64
	rowFreq := make([]int, len(m))
	for _, row := range rows {
		for _, x := range row {
			if a, ok := ref[x]; ok {
				rowFreq[a]++
				for _, y := range row {
					if b, ok := ref[y]; ok {
						m[a][b]++
					}
				}
			}
		}
	}
	return m, rowFreq
}

func makeTagList(rows [][]string) []frequency {
	m := make(map[string]int)
	for i := range rows {
		for j := range rows[i] {
			m[rows[i][j]]++
		}
	}
	freq := make([]frequency, 0, len(m))
	for k, v := range m {
		freq = append(freq, frequency{tag: k, count: v})
	}
	sort.Slice(freq, func(i, j int) bool { return freq[i].count > freq[j].count })
	if len(freq) < numTags {
		errorExit(errors.New("numTags > dataset length"))
	}
	return freq[:numTags]
}

func loadRows(data [][]string) [][]string {
	rows := make([][]string, len(data))
	for i, row := range data {
		rows[i] = strings.Split(row[colTag], " ")
	}
	return rows
}

func loadData(dataFile string) [][]string {
	f, err := os.Open(dataFile)
	if err != nil {
		errorExit(err)
	}
	reader := csv.NewReader(bufio.NewReader(f))
	if err != nil {
		errorExit(err)
	}
	_, err = reader.Read() // Discard header row
	if err != nil {
		errorExit(err)
	}
	data, err := reader.ReadAll()
	if err != nil {
		errorExit(err)
	}
	return data
}

func errorExit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

type frequency struct {
	tag   string
	count int
}

type percInd struct {
	x, y, numBelow int
	val            float64
}

type rank struct {
	row   int
	score float64
}

type intPair struct {
	index, value, numBelow int
}

func writeGob(filePath string, object interface{}) error {
	file, err := os.Create(filePath)
	if err == nil {
		encoder := gob.NewEncoder(file)
		encoder.Encode(object)
	}
	file.Close()
	return err
}

func readGob(filePath string, object interface{}) error {
	file, err := os.Open(filePath)
	if err == nil {
		decoder := gob.NewDecoder(file)
		err = decoder.Decode(object)
	}
	file.Close()
	return err
}

type bitset []uint64

func makeBitset() *bitset {
	x := make(bitset, numTags/size+1)
	return &x
}

func (s *bitset) set(i uint) {
	(*s)[i/size] |= 1 << (i % size)
}

func (s *bitset) isSet(i uint) bool {
	return (*s)[i/size]&(1<<(i%size)) != 0
}

func (s *bitset) truePos(s2 *bitset) *bitset {
	result := makeBitset()
	for i := range *s {
		(*result)[i] = (*s)[i] & (*s2)[i]
	}
	return result
}

func (s *bitset) union(s2 *bitset) *bitset {
	result := makeBitset()
	for i := range *s {
		(*result)[i] = (*s)[i] | (*s2)[i]
	}
	return result
}

func (s *bitset) difference(s2 *bitset) *bitset {
	result := makeBitset()
	for i := range *s {
		(*result)[i] = (*s)[i] ^ (*s2)[i]
	}
	return result
}

func (s *bitset) trueNeg(s2 *bitset) *bitset {
	result := makeBitset()
	for i := range *s {
		(*result)[i] = ^(*s)[i] & ^(*s2)[i]
	}
	return result
}

func (s *bitset) falsePos(s2 *bitset) *bitset {
	result := makeBitset()
	for i := range *s {
		(*result)[i] = (*s)[i] &^ (*s2)[i]
	}
	return result
}

func (s *bitset) falseNeg(s2 *bitset) *bitset {
	result := makeBitset()
	for i := range *s {
		(*result)[i] = ^(*s)[i] & (*s2)[i]
	}
	return result
}

func (s *bitset) popcnt() int {
	var sum int
	for _, x := range *s {
		sum += bits.OnesCount64(x)
	}
	return sum
}

// Matthews correlation coefficient
func mcc(truePos, trueNeg, falsePos, falseNeg int) float64 {
	tP := float64(truePos)
	tN := float64(trueNeg)
	fP := float64(falsePos)
	fN := float64(falseNeg)
	return (tP*tN - fP*fN) /
		math.Sqrt((tP+fP)*(tP+fN)*(tN+fP)*(tN+fN))
}
