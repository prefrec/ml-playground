# Tags Cosine TF-IDF

Matches items by the similarity of their tags.

## Process

* Create an n x n 2D array composed of the frequency of times tag x and y occur together on same item.
* For each x y coordinate, calculate their cosine similarity based on the vectors associated with each.
* Standardize these values, adjust with TF-IDF, and then standardize again.
* Remove pairs that do not link to each other. In other words, the value at coordinate x and y as well as coordinate y and x should both exceed a threshold.
* Convert table into a bitset, where a set bit indicates that the tag at that column exceeds a threhold.
* For an item, merge all of its tags' bitsets together.
* When scoring how closely two items match, calculate their sets' Matthews correlation coefficient.

## Notes

* You will need to install Go.
* The tables need to be built on the first run with the`-b` parameter:
  * ```go run main.go -b```
* The `-l` parameter will print a list of tag links.
